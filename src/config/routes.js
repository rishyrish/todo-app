import ListView from '../screens/ListView'
import AddTask from '../screens/AddTask'
import AddUser from '../screens/AddUser'
import LoginScreen from '../screens/Login'

const Routes = 
{
    Home: {
        screen: HomePage ,
        navigationOptions: () => ({
          header: null,
          
        })
    },
    List: {
        screen: ListView ,
        navigationOptions: () => ({
            headerBackTitleVisible: false,
        })
    },
    NewTask: {
        screen: AddTask 
    },
    NewUser: {
        screen: AddUser
    }
}

export default Routes;