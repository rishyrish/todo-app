import { 
  ADD_USER
} from '../actions/index'


export function userid(state = [], action) {
  switch (action.type) {
      case ADD_USER :
          return action.id 
      default:
          return state;
  }
}