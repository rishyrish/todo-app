import { 
    SET_VISIBILITY_FILTER,
    VisibilityFilters 
} from '../actions/index'


const { SHOW_NAME } = VisibilityFilters

export function visibilityFilter(state = SHOW_NAME, action) {
    switch (action.type) {
      case SET_VISIBILITY_FILTER:
        return action.filter
      default:
        return state
    }
}
