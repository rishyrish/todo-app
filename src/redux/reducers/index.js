import { combineReducers } from 'redux'
import { createStore } from 'redux'

import { visibilityFilter } from './tasks';
import { userid } from './users'

const todoApp = combineReducers({
    visibilityFilter,
    userid
  })
  

const store = createStore(todoApp)

export default store;
