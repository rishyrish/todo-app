
export const ADD_USER = 'ADD_USER'
// export const ADD_AGE = 'ADD_AGE'
// export const ADD_NAME = 'ADD_NAME'
// export const TOGGLE_TODO = 'TOGGLE_TODO'
export const SET_VISIBILITY_FILTER = 'SET_VISIBILITY_FILTER'

export const VisibilityFilters = {
    SHOW_PRIORITY: 'SHOW_PRIORITY',
    SHOW_NAME: 'SHOW_NAME',
    SHOW_DUEDATE: 'SHOW_DUEDATE'
}


export function addUser(id) {


    // endPoint='http://192.168.1.36:3000/api/users/';
    // requestURL = endPoint + this.state.username + '/exists';
    // console.log(requestURL);
    
    // fetch(requestURL, {
    //       method: 'GET' ,
    //       headers: {
    //         Accept: 'application/json' ,
    //       }
    //     })
    //     .then((response) => response.json())
    //     .then((responseJson) => {
    //       //console.log(responseJson.exists);
    //       if(responseJson.exists)
    //         this.props.navigation.navigate('List')
    //       else
    //         alert(" This username doesnt exists. Please try again! ");
    //     })
    //     .catch((error) => {
    //       console.error(error);
    // });
    // var c="sucess"
    return { type: ADD_USER , id};
 }
 
//  export function addAge(age) {
//     return { type: ADD_AGE, age};
//  }
 
//  export function addName(fullname) {
//     return { type: ADD_NAME, fullname};
//  }

//  export function toggleTodo(index) {
//      return { type: TOGGLE_TODO, index }
//  }

export function setVisibilityFilter(filter) {
    return { type: SET_VISIBILITY_FILTER, filter }
}