import React , { Component } from 'react';
import { Text, View , StyleSheet ,TextInput ,ScrollView, TouchableHighlight} from 'react-native';
import { Form, FormItem } from 'react-native-form-validation';

export default class AddTask extends Component {
    
    constructor(props){
        super(props);

        this.state = {
            name: null,
            details: null,
            user: 7204921675,
            duedate: null
        };
    }

    addTask() {
        const endPoint=`http://192.168.1.36:3000/api/tasks`;

        fetch( endPoint, {
            method: 'POST' ,
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
            },
            body: JSON.stringify({
                "taskName": this.state.name,
                "details": this.state.details,
                "userid": this.state.user 
            })
        })
        .then(res => res.json())
        .then(res => {
            console.log(res);
            this.props.navigation.navigate('List');
        })
        .catch(error => {
            console.log(error)
        });
    }

    render() {
        return (
        <ScrollView style={styles.scroll}>
            <View>
                <Form
                ref="form"
                shouldValidate={true}
                >    
                    <FormItem
                    isRequired={true}>
                        <TextInput
                            style={styles.textInput}
                            placeholder="Enter Task name.."
                            onChangeText={(text) => this.setState({name: text})} value={this.state.name}          
                        />
                    </FormItem>
                
                    <FormItem
                    isRequired={true}>
                        <TextInput
                            style={styles.textInput}
                            placeholder="Enter the details..."
                            onChangeText={(text) => this.setState({details: text})} value={this.state.details}           
                        />
                    </FormItem>

                    <FormItem>
                        <TextInput
                            style={styles.textInput}
                            placeholder="Due date..."
                            onChangeText={(text) => this.setState({duedate: text})} value={this.state.duedate}           
                        />
                    </FormItem>
                </Form>   

            </View>

            <View style={styles.footer} >
                <TouchableHighlight
                    style={styles.primaryButton}
                    onPress={()=> this.addTask() }
                >
                    <Text style={styles.buttonWhiteText}> CREATE</Text>
                </TouchableHighlight>
            </View>

        </ScrollView>
    
        );
    }

}

const styles= StyleSheet.create({
    scroll: {
      padding: 30,
      flexDirection: 'column',
      backgroundColor: 'white'
    },
    alignRight: {
        alignSelf: 'flex-end'
    },
    textInput: {
      height: 80,
      fontSize: 30,
      backgroundColor: '#FFF'
    },
    buttonWhiteText: {
      color: '#FFF',
      fontSize: 20
    },
    primaryButton: {
        backgroundColor: '#34A853', 
        alignItems: 'center',
        justifyContent: 'center',
        padding: 10,
        margin: 25
    },
    footer: {
      marginTop: 10
    }
  });