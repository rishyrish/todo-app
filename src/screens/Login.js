import React from 'react';
import { Text, View , StyleSheet, TextInput, ScrollView, TouchableHighlight, Image } from 'react-native';
import { Form, FormItem } from 'react-native-form-validation';

import { connect } from 'react-redux';
import { addUser, setVisibilityFilter } from '../redux/actions/index';

class LoginScreen extends React.Component {

  constructor(props){
    super(props);
 
    this.state = {
       username: null,
       status: null,
    }
  }

  componentWillReceiveProps(next) 
  {
    this.setState({
      status: next.userid
    });
    
  }

  render() {
      return (
        <ScrollView style={styles.scroll}>
          <View  style={ styles.imageLogo }> 
              <Image  size={10} source={require("../images/index.png")}  />
          </View>

          <View>
            <Form
              ref="form"
              shouldValidate={true}
              style={styles.flex}
            >
                <Text
                  style={styles.label}>
                  Enter Phone no.
                </Text>

           
                <FormItem
                  isRequired={true}>
                  <TextInput
                    style={styles.textInput}
                    keyboardType="numeric"
                    placeholder="Username"
                    onChangeText={(text) => this.setState({username: text})} value={this.state.username}          
                  />
                </FormItem>

            </Form>   

          </View>

          <View>
              <TouchableHighlight
                style={styles.primaryButton}
                onPress={()=> this.props.onUserAdded(this.state.username) }
              >
                  <Text style={styles.buttonWhiteText}> SIGN IN</Text>
              </TouchableHighlight>
          </View>
            

          <View style={styles.footer} >

              <Text>
                Haven't signed up ?
              </Text>
              <Text style={{color: 'blue'}}
                    onPress={() => this.props.navigation.navigate('NewUser')}>
               Sign up
              </Text>
          </View>


        </ScrollView>

      );
  }
}

const mapStateToProps= (state) => ({
  visibiltyFilter: state.visibiltyFilter,
  userid: state.userid
})

const mapDispatchToProps = (dispatch) => ({
  onUserAdded: (id) => dispatch(addUser(id)),
  onFilterChanged: (filter) => dispatch(setVisibilityFilter(filter)),
})

export default connect(mapStateToProps,mapDispatchToProps)(LoginScreen);


const styles= StyleSheet.create({
  scroll: {
    padding: 30,
    flexDirection: 'column',
    backgroundColor: 'white'
  },
  imageLogo: {
    justifyContent: 'center',
    alignItems:'center', 
  },
  label: {
    color: '#0d8898',
    fontSize: 30,
    marginTop: 15
  },
  alignRight: {
      alignSelf: 'flex-end'
  },
  textInput: {
    height: 80,
    fontSize: 25,
    backgroundColor: '#FFF'
  },
  buttonWhiteText: {
    color: '#FFF',
    fontSize: 20
  },
  primaryButton: {
      backgroundColor: '#34A853', 
      alignItems: 'center',
      justifyContent: 'center',
      padding: 10,
      margin: 25
  },
  footer: {
    marginTop: 10,
    alignItems: 'center',
    justifyContent: 'center',
  }
})