import React , { Component } from 'react';
import { Text, View , StyleSheet ,TextInput ,ScrollView, TouchableHighlight} from 'react-native';
import { Form, FormItem } from 'react-native-form-validation';


export default class AddUser extends Component {

	constructor(props){
        super(props);
        
        this.state = {
            name: null,
            age: 0,
            phone: null,
        };
    }	

    PushUser() {

    }

/*
    componentWillReceiveProps(next) 
    {
        if(next.dasdffg) {
            
        }
    }
*/
    render() {
    	return (
    		<ScrollView style={styles.scroll}>
            <View>
                <Form
                ref="form"
                shouldValidate={true}
                >    

                	<FormItem
                    isRequired={true}>
                        <TextInput
                            style={styles.textInput}
                            placeholder="Phone number (required) "
                            keyboardType="numeric"
                            //onChangeText={(text) => this}          
                        />
                    </FormItem>

                    <FormItem>
                        <TextInput
                            style={styles.textInput}
                            placeholder="Name"
                            //onChangeText={(text) => this.reduce( addName(text))}        
                        />
                    </FormItem>
                

                    <FormItem>
                        <TextInput
                            style={styles.textInput}
                            placeholder="Age"
                            keyboardType="numeric"
                            //onChangeText={(text) => this.reduce( addAge(text ))}          
                        />
                    </FormItem>
                </Form>   

            </View>

            <View style={styles.footer} >
                <TouchableHighlight
                    style={styles.primaryButton}
                    onPress={()=> this.PushUser(this.state) }
                >
                    <Text style={styles.buttonWhiteText}> REGISTER </Text>
                </TouchableHighlight>
            </View>

        </ScrollView>
    	);
    }

}

/*
const mapStateToProps = state => {
    
}

const mapDispatchToProps = {
    putUser
  };

export default connect(mapStateToProps, mapDispatchToProps)(AddUser);
*/


const styles= StyleSheet.create({
    scroll: {
      padding: 30,
      flexDirection: 'column',
      backgroundColor: 'white'
    },
    alignRight: {
        alignSelf: 'flex-end'
    },
    textInput: {
      height: 80,
      fontSize: 30,
      backgroundColor: '#FFF'
    },
    buttonWhiteText: {
      color: '#FFF',
      fontSize: 20
    },
    primaryButton: {
        backgroundColor: '#34A853', 
        alignItems: 'center',
        justifyContent: 'center',
        padding: 10,
        margin: 25
    },
    footer: {
      marginTop: 10
    }
  });
