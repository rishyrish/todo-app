import React , { Component } from 'react';
import { Text, View , StyleSheet , TouchableHighlight} from 'react-native';
import { List, ListItem } from 'react-native-elements'
import FlatList from 'FlatList';

export default class ListView extends Component {

    constructor(props){
        super(props);

        this.state = {
            user: 123456789,
            loading: false,
            tasks: []
        };
    }

    createTask()
    {
        console.log("im here");
        this.props.navigation.navigate('NewTask');
    }

    componentDidMount() {
        this.makeRemoteRequest();
    }

    makeRemoteRequest = () => {
        const endPoint = `http://192.168.1.36:3000/api/users/`;
        url=endPoint + this.state.user + `/tasks`;
        fetch(url, {
            method: 'GET' ,
            headers: {
              Accept: 'application/json' ,
            }
        })
        .then(res => res.json())
        .then(res => {
            this.setState({
                tasks: res,
                loading: true
            });
        //console.log(this.state.tasks[0].taskName);
        })
        .catch(error => {
            console.log(error)
        })
    };

    render () {
        return (

        <View>
            <View style={styles.header} >
              <TouchableHighlight
                style={styles.primaryButton}
                onPress={()=> this.createTask() }
              >
                  <Text style={styles.buttonWhiteText}> New Task</Text>
              </TouchableHighlight>
            </View>

            <List>
                <FlatList
                    data={this.state.tasks}
                    keyExtractor={item => item.details}
                    renderItem={({ item }) => (
                        <ListItem
                            title={item.taskName} 
                            subtitle={item.details}
                        />

                    )}
                />
            </List>
        </View>
            
        );
    }
}

const styles = StyleSheet.create({

    primaryButton: {
        backgroundColor: '#34A853', 
        alignItems: 'center',
        justifyContent: 'center',
        padding: 10,
        margin: 25
    },
    buttonWhiteText: {
        color: '#FFF',
        fontSize: 20
    },
    header: {
        //marginBottom : 10
    }

})