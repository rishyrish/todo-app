import React, { Component } from 'react';
 
import {
  StyleSheet,
  Text,
  TouchableHighlight,
} from 'react-native';
 
const Button = (props) => {
     
    function getContent(){
        if(props.children){
            return props.children;
        }
        return <Text style={styles.label}>{props.label}</Text>
    }
 
    return (
        <TouchableOpacity 
            underlayColor="#ccc"
            onPress={props.onPress} 
            style={[
                props.noDefaultStyles ? '' : styles.button, 
                props.styles ? props.styles.button : '']}
        >
            { getContent() }
        </TouchableOpacity>
    );
}
 
const styles = StyleSheet.create({
    button: {
        alignItems: 'center',
        justifyContent: 'center',
        padding: 20
    },
    label: {
        color: '#0d8898',
        fontSize: 20
    }
});
 
export default Button;