import React from 'react';
import { Provider } from 'react-redux'
import { createStackNavigator } from 'react-navigation';

import store from './src/redux/reducers/index' 
import Routes from './src/config/routes'

import ListView from './src/screens/ListView'
import AddTask from './src/screens/AddTask'
import AddUser from './src/screens/AddUser'
import LoginScreen from './src/screens/Login'

class HomePage extends React.Component {

  constructor(props){
    super(props);
  }

  render() {
    return (
        <Provider store={store}>
          <LoginScreen />
        </Provider>
    );
  }
}

const RootStack = createStackNavigator (
  Routes,
  {
    initialRouteName: 'Home',
  }
);

// RootStack.router.getStateForAction

export default class App extends React.Component {

  render() {
    return <RootStack />;
  }
}